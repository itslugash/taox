﻿# Capital City
	capital = 279
# Order of Battle
	oob = "CHL_1936"
# Research Slots
# Stability
# War Support
# Technology
	set_technology = {
		infantry_weapons = 1
		gw_artillery = 1
		early_fighter = 1
		cv_early_fighter = 1
		early_bomber = 1
		early_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
		early_battleship = 1
		transport = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
	set_country_flag = monroe_doctrine
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 9.2
			}

			centre = { 
				popularity = 88.2
			}

			right-wing = {
				popularity = 2.6
			}

			syncretic = { 
				popularity = 0
			}
		}
		
		ruling_party = centre
		last_election = "1932.10.30"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
		create_country_leader = {
			name = "Carlos Contreras Labarca"
			desc = "CHL_Carlos_Contreras_Labarca_desc"
			picture = "Portrait_CHL_Carlos_Contreras_Labarca.dds"
			expire = "1965.1.1"
			ideology = communism
			traits = {
				#
			}
		}
	# Centre
		create_country_leader = {
			name = "Arturo Alessandri"
			desc = "CHL_Arturo_Alessandri_desc"
			picture = "Portrait_CHL_Arturo_Alessandri.dds"
			expire = "1965.1.1"
			ideology = liberalism
			traits = {
				#
			}
		}
	# Right-Wing
		create_country_leader = {
			name = "Jorge González von Marées"
			desc = "CHL_Jorge_Gonzalez_von_Marees_desc"
			picture = "Portrait_CHL_Jorge_Gonzalez_von_Marees.dds"
			expire = "1965.1.1"
			ideology = fascism
			traits = {
				#
			}
		}
	# Syncretic
# Military Leaders
	# Field Marshal(s)
	# General(s)
		create_corps_commander = {
			name = "Óscar Escudero"
			portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_5.dds"
			traits = {
				trait_mountaineer
			}
			skill = 4
			attack_skill = 4
			defense_skill = 2
			planning_skill = 4
			logistics_skill = 3
		}
	# Admiral(s)
# Variants
	create_equipment_variant = {
		name = "Capitán O`Brien Class"
		type = submarine_1
		upgrades = {
			ship_reliability_upgrade = 1
			sub_engine_upgrade = 1
			sub_stealth_upgrade = 1
			sub_torpedo_upgrade = 1
		}
	}
	create_equipment_variant = {
		name = "Serrano Class"
		type = destroyer_1
		upgrades = {
			ship_torpedo_upgrade = 1
			destroyer_engine_upgrade = 1
			ship_ASW_upgrade = 1
			ship_anti_air_upgrade = 1
		}
	}
set_convoys = 20