﻿# Capital City
	capital = 292
# Order of Battle
	oob = "SAU_1936"
# Research Slots
# Stability
# War Support
# Technology
	set_technology = {
		infantry_weapons = 1
		gwtank = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 0
			}

			centre = { 
				popularity = 0
			}

			right-wing = {
				popularity = 0
			}

			syncretic = { 
				popularity = 100
			}
		}
		
		ruling_party = syncretic
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
	# Centre
	# Right-Wing
	# Syncretic
		create_country_leader = {
			name = "Ibn Saud"
			desc = "SAU_Ibn_Saud_desc"
			picture = "Portrait_SAU_Ibn_Saud.dds"
			expire = "1965.1.1"
			ideology = monarchism
			traits = {
				#
			}
		}
# Military Leaders
	# Field Marshal(s)
	# General(s)
	# Admiral(s)
# Variants
set_convoys = 5