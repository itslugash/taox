﻿# Capital City
	capital = 306
# Order of Battle
	oob = "COL_1936"
# Research Slots
# Stability
# War Support
# Technology
	set_technology = {
		infantry_weapons = 1
		tech_support = 1		
		tech_engineers = 1
		gw_artillery = 1
		early_fighter = 1
		cv_early_fighter = 1
		early_destroyer = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
	set_country_flag = monroe_doctrine
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 0
			}

			centre = { 
				popularity = 100
			}

			right-wing = {
				popularity = 0
			}

			syncretic = { 
				popularity = 0
			}
		}
		
		ruling_party = centre
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
	# Centre
		create_country_leader = {
			name = "Alfonso López Pumarejo"
			desc = "COL_Alfonso_Lopez_Pumarejo_desc"
			picture = "Portrait_COL_Alfonso_Lopez_Pumarejo.dds"
			expire = "1965.1.1"
			ideology = liberalism
			traits = {
				#
			}
		}
	# Right-Wing
	# Syncretic
# Military Leaders
	# Field Marshal(s)
	# General(s)
		create_corps_commander = {
			name = "Gustavo Rojas Pinilla"
			portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_3.dds"
			traits = {
				trickster
				urban_assault_specialist
			}
			skill = 3
			attack_skill = 3
			defense_skill = 1
			planning_skill = 3
			logistics_skill = 3
		}
	# Admiral(s)
# Variants
set_convoys = 10