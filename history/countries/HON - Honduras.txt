﻿# Capital City
	capital = 312
# Order of Battle
	oob = "HON_1936"
# Research Slots
# Stability
# War Support
	set_war_support = 0.1
# Technology
	set_technology = {
		infantry_weapons = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
	set_country_flag = monroe_doctrine
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 0
			}

			centre = { 
				popularity = 0
			}

			right-wing = {
				popularity = 0
			}

			syncretic = { 
				popularity = 0
			}
		}
		
		ruling_party = syncretic
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
	# Centre
		create_country_leader = {
			name = "Tiburcio Carías Andino"
			desc = "HON_Tiburcio_Carias_Candino_desc"
			picture = "Portrait_HON_Tiburcio_Carias_Candino.dds"
			expire = "1965.1.1"
			ideology = conservatism
			traits = {
				#
			}
		}
	# Right-Wing
	# Syncretic
# Military Leaders
	# Field Marshal(s)
	# General(s)
	# Admiral(s)
# Variants
set_convoys = 10

