﻿# Capital City
	capital = 327
# Order of Battle
	oob = "PHI_1936"
# Research Slots
# Stability
# War Support
# Technology
	set_technology = {
		infantry_weapons = 1
		infantry_weapons1 = 1
		tech_support = 1		
		tech_engineers = 1
		gw_artillery = 1
		early_fighter = 1
		early_bomber = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 32.01
			}

			centre = { 
				popularity = 67.99
			}

			right-wing = {
				popularity = 0
			}

			syncretic = { 
				popularity = 0
			}
		}
		
		ruling_party = centre
		last_election = "1935.9.16"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
		create_country_leader = {
			name = "Emilio Aguinaldo"
			desc = "PHI_Emilio_Aguinaldo_desc"
			picture = "Portrait_PHI_Emilio_Aguinaldo.dds"
			expire = "1965.1.1"
			ideology = socialism
			traits = {
				#
			}
		}
	# Centre
		create_country_leader = {
			name = "Manuel L. Quezon"
			desc = "PHI_Manuel_L_Quezon_desc"
			picture = "Portrait_PHI_Manuel_L_Quezon.dds"
			expire = "1965.1.1"
			ideology = conservatism
			traits = {
				#
			}
		}
	# Right-Wing
	# Syncretic
# Military Leaders
	# Field Marshal(s)
	# General(s)
	# Admiral(s)
# Variants
set_convoys = 5