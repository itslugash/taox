﻿# Capital City
	capital = 545
# Order of Battle
	oob = "ZIM_1936"
# Research Slots
# Stability
# War Support
# Technology
	set_technology = {
		infantry_weapons = 1
		tech_mountaineers = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 0
			}

			centre = { 
				popularity = 0
			}

			right-wing = {
				popularity = 0
			}

			syncretic = { 
				popularity = 100
			}
		}
		
		ruling_party = syncretic
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
	# Centre
	# Right-Wing
	# Syncretic
# Military Leaders
	# Field Marshal(s)
	# General(s)
	# Admiral(s)
# Variants
set_convoys = 5