﻿# Capital City
	capital = 328
# Order of Battle
	oob = "MAN_1936"
# Research Slots
# Stability
# War Support
# Technology
	set_technology = {
		infantry_weapons = 1
		gw_artillery = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 0
			}

			centre = { 
				popularity = 0
			}

			right-wing = {
				popularity = 100
			}

			syncretic = { 
				popularity = 0
			}
		}
		
		ruling_party = right-wing
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
	# Centre
	# Right-Wing
		create_country_leader = {
			name = "Puyi"
			desc = "MAN_Puyi_desc"
			picture = "Portrait_MAN_Puyi.dds"
			expire = "1967.11.17"
			ideology = nationalism
			traits = {
				warrior_code
			}
		}
	# Syncretic
# Military Leaders
	# Field Marshal(s)
	# General(s)
		create_corps_commander = {
			name = "Aisin Gioro Xiqia"
			GFX = "GFX_portrait_man_aisin_gioro_xiqia"
			traits = {
				career_officer
			}
			skill = 3
			attack_skill = 3
			defense_skill = 3
			planning_skill = 3
			logistics_skill = 1
		}
		create_corps_commander = {
			name = "Yoshiko Kawashima"
			gfx = "GFX_portrait_man_yoshiko_kawashima"
			traits = {
				cavalry_officer
			}
			skill = 3
			attack_skill = 3
			defense_skill = 3
			planning_skill = 2
			logistics_skill = 2
			female = yes
		}
		create_corps_commander = {
			name = "Zhang Haipeng"
			GFX = "GFX_portrait_man_zhang_haipeng"
			traits = {
				#
			}
			skill = 2
			
			attack_skill = 2
			defense_skill = 2
			planning_skill = 1
			logistics_skill = 2
		}
		create_corps_commander = {
			name = "Zhang Jinghui"
			GFX = "GFX_portrait_man_zhang_jinghui"
			traits = {
				infantry_officer
			}
			skill = 2
			
			attack_skill = 2
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 2
		}
	# Admiral(s)
# Variants
if = {
	limit = {
		has_dlc = "Waking the Tiger"
	}
	set_country_flag = MAN_northern_bandits
	set_country_flag = MAN_eastern_bandits
	set_country_flag = MAN_western_bandits
	add_ideas = MAN_banditry
	add_ideas = MAN_kwantung_veto
	add_ideas = MAN_low_legitimacy_5
}
set_convoys = 5

 # Hsinking - Manchuria