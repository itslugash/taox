﻿# Capital City
	capital = 43
# Order of Battle
	oob = "HUN_1936"
# Research Slots
	set_research_slots = 3
# Stability
	set_stability = 0.7
# War Support
	set_war_support = 0.6
# Technology
	set_technology = {
		infantry_weapons = 1
		infantry_weapons1 = 1
		tech_support = 1		
		tech_recon = 1
		gw_artillery = 1
		gwtank = 1
		early_fighter = 1
		CAS1 = 1
	}
# Diplomacy
	# Factions
	# Puppets
	# Tech Sharing
# Set Politics
	set_politics = {

		parties = {
			left-wing = {
				popularity = 0
			}

			centre = { 
				popularity = 19.9
			}

			right-wing = {
				popularity = 77.3
			}

			syncretic = { 
				popularity = 2.8
			}
		}
		
		ruling_party = right-wing
		last_election = "1935.3.31"
		election_frequency = 48
		elections_allowed = no
	}
# Country Leaders
	# Left-Wing
		create_country_leader = {
			name = "Béla Kun"
			desc = "HUN_Bela_Kun_desc"
			picture = "Portrait_HUN_Bela_Kun.dds"
			expire = "1965.1.1"
			ideology = communism
			traits = {
				#
			}
		}
	# Centre
		create_country_leader = {
			name = "Miklós Horthy"
			desc = "HUN_Miklos_Horthy_desc"
			picture = "Portrait_HUN_Miklos_Horthy.dds"
			expire = "1965.1.1"
			ideology = social_democracy
			traits = {
				#
			}
		}
	# Right-Wing
		create_country_leader = {
			name = "Miklós Horthy"
			desc = "HUN_Miklos_Horthy_desc"
			picture = "Portrait_HUN_Miklos_Horthy.dds"
			expire = "1965.1.1"
			ideology = fascism
			traits = {
				#
			}
		}
	# Syncretic
		create_country_leader = {
			name = "Miklós Horthy"
			desc = "HUN_Miklos_Horthy_desc"
			picture = "Portrait_HUN_Miklos_Horthy.dds"
			expire = "1965.1.1"
			ideology = monarchism
			traits = {
				#
			}
		}
# Military Leaders
	# Field Marshal(s)
		create_field_marshal = {
			name = "Ferenc Feketehalmy-Czeydner"
			gfx = GFX_Portrait_hungary_ferenc_feketehalmy_czeydner
			traits = {
				#
			}
			skill = 2
			attack_skill = 2
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 2
		}
	# General(s)
		create_corps_commander = {
			name = "Iván Hindy"
			gfx = GFX_Portrait_hungary_ivan_hindy
			traits = {
				trickster
			}
			skill = 4
			attack_skill = 3
			defense_skill = 4
			planning_skill = 2
			logistics_skill = 4
		}
		create_corps_commander = {
			name = "Géza Lakatos"
			gfx = GFX_Portrait_hungary_geza_lakatos
			traits = {
				#
			}
			skill = 3
			attack_skill = 3
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 2
		}
		create_corps_commander = {
			name = "Lajos Veress"
			gfx = GFX_Portrait_hungary_lajos_veress
			traits = {
				armor_officer
			}
			skill = 2
			attack_skill = 2
			defense_skill = 1
			planning_skill = 2
			logistics_skill = 2
		}
		create_corps_commander = {
			name = "Károly Beregfy"
			gfx = GFX_Portrait_hungary_karoly_beregfy
			traits = {
				#
			}
			skill = 1
			attack_skill = 1
			defense_skill = 1
			planning_skill = 1
			logistics_skill = 1
		}
	# Admiral(s)
# Variants
if = {
	limit = {
		has_dlc = "Death or Dishonor"
	}
	add_ideas = {
		disarmed_nation
		HUN_treaty_of_triannon
	}
}